# speedindex-blackbox

A prometheus probe exporter similar to the [prometheus blackbox probe exporter](https://github.com/prometheus/blackbox_exporter)
except it measures [SpeedIndex](https://sites.google.com/a/webpagetest.org/docs/using-webpagetest/metrics/speed-index) metrics 
instead of network response times.

All the hard-work is done by [Browsertime](https://github.com/sitespeedio/browsertime) under the hood.

# Configuration

The configuration file is fairly straight-forward:

```yaml
# The list of URLs you would like to probe
urls:
  - https://gitlab.com/gitlab-org/gitlab-ce
  - https://gitlab.com/gitlab-org/gitlab-ce/issues
  - https://gitlab.com/gitlab-org/gitlab-ce/merge_requests
  - https://gitlab.com/dashboard/todos
  - https://gitlab.com/gitlab-org/gitlab-ce/commits/master

# Browsers you would like to test in. See browsertime for details. 
browsers:
  - firefox
  - chrome

# Connection profiles to use in probes. See browsertime for details.
connections:
  - native
  - cable
  - 3g

# How long between successive probes
config:
  iteration_delay_s: 1
```

# Running

The easiest way to play with this setup is through the supplied docker-compose file.

```shell
docker-compose up --build
```

The visit [http://localhost:9090/](http://localhost:9090/) to interact with Prometheus.

# Prometheus Metrics

The speedindex results are exposed as Prometheus histogram metrics with labels for
`url`, `connection` and `browser`.

### Graphs

You should be able to generate graphs like this:

![](https://gitlab.com/andrewn/speedindex-blackbox/uploads/362d353e2c52d1fbbf9a8d794c7e367e/image.png)

# Useful Prometheus Queries

#### Overall Median speedindex value

```
 histogram_quantile(0.50, sum(speedindex_time_bucket) by (le)) 
```

#### Median speedindex values for a single URL at native speeds, by connection

```
 histogram_quantile(0.50, sum(speedindex_time_bucket) by (le, connection)) 
```


#### Median speedindex values for a single URL at native speeds, by browser

```
 histogram_quantile(0.50, sum(speedindex_time_bucket{connection="native", url="https://gitlab.com/gitlab-org/gitlab-ce"}) by (le, browser)) 
```

#### 99th Percentile speedindex values for a single URL at native speeds, by browser

```
 histogram_quantile(0.99, sum(speedindex_time_bucket{connection="native", url="https://gitlab.com/gitlab-org/gitlab-ce"}) by (le, browser)) 
```

