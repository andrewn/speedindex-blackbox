#!/usr/bin/env node

'use strict';

const client = require('prom-client');
const httpProm = require('./http-prom');
const browserTime = require('./browsertime');
const yaml = require('js-yaml');
const fs = require('fs');

const speedIndexHistogram = new client.Histogram({
  name: 'speedindex_time',
  help: 'speed index',
  labelNames: ['url', 'connection', 'browser'],
  buckets: [0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
});

const speedIndexGauge = new client.Gauge({
  name: 'speedindex_value',
  help: 'Speedindex',
  labelNames: ['url', 'connection', 'browser'],
});

const failureCounter = new client.Counter({
  name: 'speedindex_failures',
  help: 'failures',
  labelNames: ['url', 'connection', 'browser'],
});

console.log('Starting...')

const config = yaml.safeLoad(fs.readFileSync('config.yaml', 'utf8'));

const delay = config.config.iteration_delay * 1000;

function seq() {
  const urls = config.urls;
  const connections = config.connections;
  const browsers = config.browsers;

  let i = 0;

  return () => {
    var c = i++;
    const url = urls[c % urls.length];
    c = Math.floor(c / urls.length);

    const connection = connections[c % connections.length];
    c = Math.floor(c / connections.length);

    const browser = browsers[c % browsers.length];
    return { url, connection, browser };
  }
}

var next = seq();

function query({ url, connection, browser }) {
  return browserTime({ url, connection, browser })
    .then(result => {
      if (!result) throw new Error('No results');

      console.log(`url=${url}, connection=${connection}, browser=${browser}, speedindex=${result.SpeedIndex}`)
      speedIndexHistogram.observe({ url, connection, browser }, result.SpeedIndex);
      speedIndexGauge.set({ url, connection, browser }, result.SpeedIndex);
    })
    .catch(reason => {
      console.error(reason);
      failureCounter.inc({ url, connection, browser });
    });
}

httpProm(client.register)

function nextIter() {
  const details = next();

  query({ url: details.url, connection: details.connection, browser: details.browser })
    .then(() => {
      // Do not chain to previous promise
      setTimeout(nextIter, delay);
    })
}

nextIter();
