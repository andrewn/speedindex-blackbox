'use strict';

const http = require('http');

function exposePrometheus(register) {
  const port = parseInt(process.env.PORT, 10) || 9922;

  const server = http.createServer((request, response) => {
    // Overly simplistic but good enough for now
    response.end(register.metrics())
  })

  server.listen(port, (err) => {
    if (err) {
      console.error(err);
      process.exit(1)
    }
  })
}

module.exports = exposePrometheus;
