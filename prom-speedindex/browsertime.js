'use strict';

const Promise = require('bluebird');
const rimraf = require('rimraf');
const { spawn } = require('child_process');
const glob = require('glob');
var fs = require('fs');

function spawnBrowserTimeProcess({ url, connection, browser }) {
  let child;

  return new Promise((resolve, reject) => {
    const args =  ['--iterations', 1, '-c', connection, '--speedIndex', '--skipHar', '--browser', browser, url];
    const processName = '/usr/src/app/bin/browsertime.js';

    console.log(`Starting ${processName} ${args.join(' ')}`)
    child = spawn(processName, args, { stdio: 'inherit' });

    child.on('error', reject);

    child.on('close', (code) => {
      if (code) {
        return reject(new Error(`Browsertime failed with ${code}`))
      }

      return resolve()
    })
  })
  .finally(() => {
    if (!child) return;
    child.kill();
    child.unref();
    child = null;
  });
}

function readBrowsertime() {
  return Promise.fromCallback(callback => glob('browsertime-results/**/browsertime.json', callback))
    .then(results => {
      if (!results || results.length !== 1) throw new Error('Unexpected files found: ', results.join(','));

      var browserFileJSON = results[0];

      return Promise.fromCallback(callback => fs.readFile(browserFileJSON, 'utf8', callback));
    })
    .then(JSON.parse);
}

function browserTime({ url, connection, browser }) {
  return Promise.fromCallback(callback => rimraf('browsertime-results', callback))
    .then(() => {
      return spawnBrowserTimeProcess({ url, connection, browser });
    })
    .then(() => {
      return readBrowsertime()
    })
    .then(json => json.visualMetrics[0]);
}

module.exports = browserTime;
